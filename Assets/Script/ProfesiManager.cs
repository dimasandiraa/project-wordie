﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfesiManager : MonoBehaviour {
	protected ManagerScript manager;
	protected string _profesi;
	protected string _imageString;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		manager = FindObjectOfType<ManagerScript>();
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
	{
		if(!manager.isOpenProfesi){
			Destroy(this.gameObject);
		}
	}

	public void getAction(){
        string profesi = this.gameObject.transform.Find("ProfesiText").gameObject.GetComponent<Text>().text;
		manager.isOpenAction=true;
        manager.generatingWord = true;
		if(manager.isMembaca){
			StartCoroutine(manager.CallingWords(profesi));
		}
		else{
			//StartCoroutine(CallingCerita());
		}
        getImage();
        //manager.imageRender(profesi);
		manager.OpenLayer();
	}

	public void getImage(){
		_profesi = this.gameObject.transform.Find("ProfesiText").gameObject.GetComponent<Text>().text;
		StartCoroutine(CallingImage());
	}

	public IEnumerator CallingImage(){
		WWWForm form = new WWWForm();
		form.AddField("profesi", _profesi);
		WWW wwwImage = new WWW("http://localhost/Wordie/get_image.php", form);
		yield return wwwImage;
        //manager.openImage(wwwImage.text);
        manager.imageRender(wwwImage.text);
        manager.OpenLayer();
	}

	/*public IEnumerator CallingCerita(){
		if(manager.isOpenAction){
			WWWForm form = new WWWForm();
			form.AddField("profesi", this.gameObject.transform.Find("ProfesiText").gameObject.GetComponent<Text>().text);
			WWW wwwCerita = new WWW("http://localhost/Wordie/get_cerita.php", form);
			yield return wwwCerita;
			string[] stringText = wwwCerita.text.Split('|');
			if(manager.isIndo){
				manager.GenerateStory(stringText[0]);
			}
			else{
				manager.GenerateStory(stringText[1]);
			}
			manager.OpenLayer();
		}
	}*/

	
}
