﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour {
	public enum LanguageState{INDONESIA, ENGLISH};
	public LanguageState state = LanguageState.INDONESIA;
	public ScrollRect scrollViewProfesi;
	public GameObject scrollContentProfesi;
	public GameObject listPrefabProfesi;
	protected string[] stringRows;
	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		StartCoroutine(CallingProfesi());
	}

	public IEnumerator CallingProfesi(){
		WWW wwwProfesi = new WWW("http://localhost/Wordie/get_profesi_rows.php");
		yield return wwwProfesi;
		stringRows = wwwProfesi.text.Split('!');
		CallGenerateProfesi();
		//string[][] stringFields;
		
	}

	public void CallGenerateProfesi(){
		for(int i = 0; i < stringRows.Length; i++){
			string[] temp = stringRows[i].Split('|');
			if(state == LanguageState.INDONESIA){
				GenerateObject(temp[0]);
			}
			else if(state == LanguageState.ENGLISH){
				GenerateObject(temp[1]);
			}
		}
		scrollViewProfesi.verticalNormalizedPosition = 1;
	}

	public void GenerateObject(string textProfesi){
		GameObject scrollitemObj = Instantiate(listPrefabProfesi);
		scrollitemObj.transform.SetParent(scrollContentProfesi.transform, false);
		scrollitemObj.transform.Find("ProfesiText").gameObject.GetComponent<Text>().text = textProfesi;
		Debug.Log(textProfesi);
	}
}
