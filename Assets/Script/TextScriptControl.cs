﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScriptControl : MonoBehaviour {

	protected ManagerScript gamemanager;

	// Use this for initialization
	void Start () {
		gamemanager = FindObjectOfType<ManagerScript>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!gamemanager.isOpenAction){
			Destroy(this.gameObject);
		}
	}
}
