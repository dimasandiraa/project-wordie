﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System.Drawing;

public class ManagerScript : MonoBehaviour {
	public enum LanguageState{INDONESIA, ENGLISH};
	public enum ActionState{MEMBACA, MENDENGARKAN};
	public LanguageState state = LanguageState.INDONESIA;
	public ActionState stateAction = ActionState.MEMBACA;
	public ScrollRect scrollViewProfesi;
	public GameObject scrollContentProfesi;
	public GameObject listPrefabProfesi;
	public ScrollRect scrollViewCerita;
	public GameObject[] contentWord;
	public GameObject prefabCerita;
	public GameObject[] prefabWord;
	public GameObject layar;
	public GameObject backButtonCanvas;
    public GameObject nextWordButton;
	public GameObject backButton;
	public GameObject gambarCanvas;
    public GameObject gambarProfesi;
	public GameObject pilihan;
    public GameObject[] audioIndo;
    public GameObject[] audioEnglish;
	public Text membaca;
	public Text mendengarkan;
	public int[] rectangleMeasure;
	public bool viewImage = false;
	public bool isIndo = true;
	public bool isMembaca = true;
	public bool isOpenProfesi = false;
	public bool isOpenAction = false;
    public bool generatingWord = false;
    public bool stoppedWord = false;
    public bool showWord = false;
	
	protected string _image;
	protected string[] stringRows;
    protected int indexWord=0;

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
	{
		checkLanguageState();
		checkActionState();
	}

    public void SetIndexWord(int i)
    {
        indexWord = i;
    }

    public int GetIndexWord()
    {
        return indexWord;
    }

    public void NextIndex()
    {
        indexWord += 2;
        Debug.Log(indexWord);
        GameObject[] gameObject = GameObject.FindGameObjectsWithTag("WordText");
        foreach(GameObject target in gameObject){
            GameObject.Destroy(target);
        }
        nextWordButton.SetActive(false);
    }

    public void PreviousIndex()
    {
        indexWord -= 1;
    }

    public void ResetIndex()
    {
        indexWord = 0;
    }

    public void GeneratingWord()
    {
        generatingWord = true;
    }

    public void StopGenerateWord()
    {
        generatingWord = false;
        StopAllCoroutines();
        ResetIndex();
    }

    public void ExitApp()
    {
        Application.Quit();
    }

	public void OpenLayer(){
		layar.SetActive(true);
		gambarCanvas.SetActive(true);
		backButtonCanvas.SetActive(true);
		backButton.SetActive(false);
        nextWordButton.SetActive(false);
		//pilihan.SetActive(false);
	}

	public void CloseLayer(){
		isOpenAction = false;
		viewImage = false;
        StopGenerateWord();
		StartCoroutine(ClosingLayer());
		//pilihan.SetActive(true);
	}

	public IEnumerator ClosingLayer(){
        yield return new WaitForSeconds(5.0f);
		layar.SetActive(false);
		gambarCanvas.SetActive(false);
		backButtonCanvas.SetActive(false);
		backButton.SetActive(true);
        nextWordButton.SetActive(false);
	}

	public void OpenProfesi(){
		isOpenProfesi = true;
	}

	public void CloseProfesi(){
		isOpenProfesi = false;
        GameObject[] profesi = GameObject.FindGameObjectsWithTag("ProfesiButton");
        foreach(GameObject target in profesi)
        {
            Destroy(target);
        }
	}

	public void checkActionState(){
		if(stateAction == ActionState.MEMBACA){
			isMembaca = true;
		}
		else if(stateAction == ActionState.MENDENGARKAN){
			isMembaca = false;
		}
	}

	public void checkLanguageState(){
		if(state == LanguageState.INDONESIA){
			membaca.text = "Membaca";
			mendengarkan.text = "Mendengarkan";
			isIndo = true;
            for(int i = 0; i < audioIndo.Length; i++)
            {
                audioIndo[i].SetActive(true);
            }
            for (int i = 0; i < audioIndo.Length; i++)
            {
                audioEnglish[i].SetActive(false);
            }
        }
		else if(state == LanguageState.ENGLISH){
			membaca.text = "Reading";
			mendengarkan.text = "Listening";
			isIndo = false;
            for (int i = 0; i < audioEnglish.Length; i++)
            {
                audioEnglish[i].SetActive(true);
            }
            for (int i = 0; i < audioIndo.Length; i++)
            {
                audioIndo[i].SetActive(false);
            }
        }
	}

	public void changeState(string newState){
		if(newState == "INDONESIA"){
			state = LanguageState.INDONESIA;
			isIndo = true;
            for (int i = 0; i < audioIndo.Length; i++)
            {
                audioIndo[i].SetActive(true);
            }
            for (int i = 0; i < audioIndo.Length; i++)
            {
                audioEnglish[i].SetActive(false);
            }
        }
		else if(newState == "ENGLISH"){
			state = LanguageState.ENGLISH;
			isIndo = false;
            for (int i = 0; i < audioEnglish.Length; i++)
            {
                audioEnglish[i].SetActive(true);
            }
            for (int i = 0; i < audioIndo.Length; i++)
            {
                audioIndo[i].SetActive(false);
            }
        }
	}

	public void changeActionState(string newState){
		if(newState == "MEMBACA"){
			stateAction = ActionState.MEMBACA;
			isMembaca = true;
		}
		else if(newState == "MENDENGARKAN"){
			stateAction = ActionState.MENDENGARKAN;
			isMembaca = false;
		}
	}

	public void openImage(string stringImage){
		_image = stringImage;
        Debug.Log(_image);
		if(_image.Length > 100){
			viewImage = true;
		}
		else{
			viewImage = false;
		}
	}

    public void imageRender(string image)
    {
        byte[] bytes = File.ReadAllBytes(@"C:\xampp\htdocs\Wordie\admin\asset\img\" + image);
        Texture2D sampleTexture = new Texture2D(2, 2);
        // the size of the texture will be replaced by image size
        bool isLoaded = sampleTexture.LoadImage(bytes);
        // apply this texure as per requirement on image or material
        //GameObject imageGameObject = GameObject.Find("RawImage");
        if (isLoaded)
        {
            //image.GetComponent<RawImage>().texture = sampleTexture;
            gambarProfesi.GetComponent<RawImage>().texture = sampleTexture;
        }
    }

    /// <summary>
    /// OnGUI is called for rendering and handling GUI events.
    /// This function can be called multiple times per frame (one call per event).
    /// </summary>
    void OnGUI()
	{
		if(viewImage){
			byte[] bytes = System.Convert.FromBase64String(_image);
			//Debug.Log(System.Convert.FromBase64String(_image));
			Texture2D textureImage = new Texture2D(1, 1);
			textureImage.LoadImage(bytes);
			textureImage.Compress(true);
			GUI.DrawTexture(new Rect(Screen.width/3, Screen.height/8, rectangleMeasure[2], rectangleMeasure[3]), textureImage, ScaleMode.ScaleToFit, true, 1f);
		}
	}

	public void getProfesi(){
		StartCoroutine(CallingProfesi());
	}

	public IEnumerator CallingProfesi(){
		WWW wwwProfesi = new WWW("http://localhost/Wordie/get_profesi_rows.php");
		yield return wwwProfesi;
		stringRows = wwwProfesi.text.Split('!');
		CallGenerateProfesi();
	}

	public void CallGenerateProfesi(){
		if(isOpenProfesi){
			for(int i = 0; i < stringRows.Length-1; i++){
				string[] temp = stringRows[i].Split('|');
				if(state == LanguageState.INDONESIA && temp[0] != null){
					GenerateObject(temp[0]);
				}
				else if(state == LanguageState.ENGLISH && temp[1] != null){
					GenerateObject(temp[1]);
				}
			}
			scrollViewProfesi.verticalNormalizedPosition = 1;
		}
	}

	public void GenerateObject(string textProfesi){
		if(textProfesi != ""){
			GameObject scrollitemObj = Instantiate(listPrefabProfesi);
			scrollitemObj.transform.SetParent(scrollContentProfesi.transform, false);
			scrollitemObj.transform.Find("ProfesiText").gameObject.GetComponent<Text>().text = textProfesi;
			scrollitemObj.SetActive(true);
		}
	}

	/*public void GenerateStory(string textStory){
		if(textStory != ""){
			GameObject textObj = Instantiate(prefabCerita);
			textObj.transform.SetParent(scrollContentCerita.transform, false);
			textObj.GetComponent<Text>().text = textStory;
		}
		scrollViewCerita.verticalNormalizedPosition = 1;
	}*/

	public void GenerateWord1(string word){
		if(word != ""){
			GameObject wordObj = Instantiate(prefabWord[Random.Range(0, prefabWord.Length)]);
			wordObj.transform.SetParent(contentWord[0].transform, false);
			wordObj.transform.Find("Text").gameObject.GetComponent<Text>().text = word;
			//wordObj.GetComponent<Text>().text = word;
		}
		scrollViewCerita.verticalNormalizedPosition = 1;
	}

	public void GenerateWord2(string word){
		if(word != ""){
			GameObject wordObj = Instantiate(prefabWord[Random.Range(0, prefabWord.Length)]);
			wordObj.transform.SetParent(contentWord[1].transform, false);
			wordObj.transform.Find("Text").gameObject.GetComponent<Text>().text = word;
			//wordObj.GetComponent<Text>().text = word;
		}
		scrollViewCerita.verticalNormalizedPosition = 1;
	}

    public IEnumerator CallingWords(string indexProfesi)
    {
        //int indexWord = GetIndexWord();
        stoppedWord = false;
        while (isOpenAction)
        {
            if (generatingWord)
            {
                WWWForm form = new WWWForm();
                form.AddField("profesi", indexProfesi);
                WWW wwwCerita = new WWW("http://localhost/Wordie/get_words.php", form);
                yield return wwwCerita;
                Debug.Log(wwwCerita.text);
                string[] stringWord = wwwCerita.text.Split('$');
                string[] word1 = stringWord[indexWord].Split('|');
                string[] word2 = stringWord[indexWord + 1].Split('|');

                if (isIndo)
                {
                    string[] tempInd1 = word1[0].Split('-');
                    string[] tempInd2 = word2[0].Split('!');
                    for (int i = 0; i < tempInd1.Length; i++)
                    {
                        yield return new WaitForSeconds(5.0f);
                        if (generatingWord)
                        {
                            if (!stoppedWord)
                            {
                                GenerateWord1(tempInd1[i]);
                            }
                            else
                            {
                                goto Exit;
                                break;
                            }
                        }
                        else
                        {
                            stoppedWord = true;
                            goto Exit;

                            break;
                        }
                    }

                    if (generatingWord && !stoppedWord)
                    {
                        yield return new WaitForSeconds(5.0f);
                        for (int i = 0; i < tempInd2.Length; i++)
                        {
                            if (generatingWord)
                            {
                                if (!stoppedWord)
                                {
                                    GenerateWord2(tempInd2[i]);
                                }
                                yield return new WaitForSeconds(7.0f);
                            }
                            else
                            {
                                stoppedWord = true;
                                goto Exit;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    string[] tempEng1 = word1[1].Split('-');
                    string[] tempEng2 = word2[1].Split('!');
                    for (int i = 0; i < tempEng1.Length; i++)
                    {
                        if (generatingWord)
                        {
                            yield return new WaitForSeconds(5.0f);
                            GenerateWord1(tempEng1[i]);
                        }
                        else if (!generatingWord)
                        {
                            Debug.Log("Break in loop");
                            break;
                        }
                    }

                    Debug.Log("Break");

                    if (generatingWord)
                    {
                        yield return new WaitForSeconds(5.0f);
                        if (generatingWord)
                        {
                            for (int i = 0; i < tempEng2.Length; i++)
                            {
                                if (generatingWord)
                                {
                                    GenerateWord2(tempEng2[i]);
                                    yield return new WaitForSeconds(7.0f);
                                }
                                else if (!generatingWord)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            Debug.Log("AFTER GENERATE WORD");
            generatingWord = false;
            nextWordButton.SetActive(true);
            yield return new WaitUntil(() => generatingWord == true);
            Exit: if (stoppedWord)
            {
                break;
            }
        }
    }
}
