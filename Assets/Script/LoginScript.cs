﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginScript : MonoBehaviour {

	public InputField usernameField;
	public InputField passwordField;
	
	public Button submitButton;

	public GameObject currentCanvas;
	public GameObject nextCanvas;

	public void CallLogin(){
		StartCoroutine(Login());
	}

	protected virtual IEnumerator Login(){
		WWWForm form = new WWWForm();
		form.AddField("username", usernameField.text);
		form.AddField("password", passwordField.text);
		WWW www = new WWW("http://localhost/Wordie/cek_user.php", form);
		yield return www;
		if(www.text[0] == '0'){
			UserManager.username = usernameField.text;
			Debug.Log("Login Successful");
			currentCanvas.SetActive(false);
			nextCanvas.SetActive(true);
		}
		else{
			Debug.Log("User login failed. Error #" + www.text);
		}
	}

	public virtual void VerifyForm(){
		submitButton.interactable = (usernameField.text.Length >= 6 && passwordField.text.Length >= 6);
	}
}
