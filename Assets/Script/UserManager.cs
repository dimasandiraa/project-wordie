using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserManager : MonoBehaviour {

    public static string username;
    public GameObject loginMenu;
    public GameObject loginCanvas;
    public GameObject bahasaMenu;
    public GameObject pilihMenu;
    public GameObject profesiMenu;
    public GameObject aboutCanvas;

	public static bool LoggedIn{ get { return username != null; } }

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        loginMenu.SetActive(true);
        loginCanvas.SetActive(false);
        bahasaMenu.SetActive(false);
        pilihMenu.SetActive(false);
        profesiMenu.SetActive(false);
        aboutCanvas.SetActive(false);
    }
    
    public static void LogOut(){
        username = null;
    }

    public virtual void CanvasLogout(){
        loginMenu.SetActive(true);
        loginCanvas.SetActive(false);
        bahasaMenu.SetActive(false);
        pilihMenu.SetActive(false);
        profesiMenu.SetActive(false);
        aboutCanvas.SetActive(false);
    }
}
